## ANAGRAMATA

An [anagram](https://en.wikipedia.org/wiki/Anagram) generator web app. Try and find as many anagrams as possible in under 5 minutes.

Try it out [here](https://anagramata.netlify.app/).
